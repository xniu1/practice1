package com.xniu.drop1;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Saying {
    private long id;
    private String context;

    public Saying() {
    }

    public Saying(long id, String context) {
        this.id = id;
        this.context = context;
    }

    @JsonProperty
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @JsonProperty
    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }
}
